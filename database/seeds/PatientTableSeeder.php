<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Faker\Factory as Faker;

class PatientTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();

        $faker = Faker::create();

        foreach (range(1,10) as $index) {
            App\Patient::create([
                'document'=> $faker->randomNumber(),
                'name'=> $faker->firstName().' '.$faker->lastName(),
                'birthday'=> $faker->date('d/m/Y')
            ]);
        }
    }
}
