<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $elements= collect([
            ['name'=> 'Tony Blanco', 'email'=> 'tblanco82@gmail.com', 'password'=> bcrypt('12345')]
        ]);

        $elements->each(function($item){

            App\User::create($item);
        });
    }
}
