<?php

use Illuminate\Database\Seeder;

class AttributeQuestionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $elements= collect([
            // DAS8
            // Pain attributes
            ['name'=> 'view', 'value'=> 'exam.das28.drawing', 'question'=>  'pain-skel'],
            ['name'=> 'placeholders', 'value'=> '{xc: 27.5, yc: 76, r: 10}, {xc: 100.5, yc: 76, r: 10}, {xc: 12, yc: 132, r: 10}, {xc: 110, yc: 132, r: 10}, {xc: 10.5, yc: 178, r: 10}, {xc: 125, yc: 175.5, r: 10}, {xc: 51, yc: 253, r: 10}, {xc: 72, yc: 253, r: 9}', 'question'=>  'pain-skel'],
            ['name'=> 'width', 'value'=> '150', 'question'=>  'pain-skel'],
            ['name'=> 'height', 'value'=> '352', 'question'=>  'pain-skel'],

            ['name'=> 'view', 'value'=> 'exam.das28.drawing', 'question'=>  'pain-right-hand'],
            ['name'=> 'placeholders', 'value'=> '{xc: 130, yc: 20, r: 8}, {xc: 155, yc: 50, r: 8}, {xc: 32, yc: 60, r: 8}, {xc: 55, yc: 67, r: 8}, {xc: 93, yc: 76, r: 8}, {xc: 22, yc: 75, r: 8}, {xc: 45, yc: 83, r: 8}, {xc: 86, yc: 95, r: 8}, {xc: 22, yc: 95, r: 8}, {xc: 45, yc: 103, r: 8}, {xc: 86, yc: 115, r: 8}, {xc: 40, yc: 122, r: 8}, {xc: 60, yc: 131, r: 8}, {xc: 88, yc: 140, r: 8}', 'question'=>  'pain-right-hand'],
            ['name'=> 'width', 'value'=> '200', 'question'=>  'pain-right-hand'],
            ['name'=> 'height', 'value'=> '166', 'question'=>  'pain-right-hand'],

            ['name'=> 'view', 'value'=> 'exam.das28.drawing', 'question'=>  'pain-left-hand'],
            ['name'=> 'placeholders', 'value'=> '{xc: 130-60, yc: 20, r: 8}, {xc: 155-110, yc: 50, r: 8}, {xc: 32+135, yc: 60, r: 8}, {xc: 55+90, yc: 67, r: 8}, {xc: 93+15, yc: 76, r: 8}, {xc: 22+156, yc: 75, r: 8}, {xc: 45+110, yc: 83, r: 8}, {xc: 86+30, yc: 95, r: 8}, {xc: 22+158, yc: 95, r: 8}, {xc: 45+108, yc: 103, r: 8}, {xc: 86+30, yc: 115, r: 8}, {xc: 40+120, yc: 122, r: 8}, {xc: 60+80, yc: 131, r: 8}, {xc: 88+25, yc: 140, r: 8}', 'question'=>  'pain-left-hand'],
            ['name'=> 'width', 'value'=> '200', 'question'=>  'pain-left-hand'],
            ['name'=> 'height', 'value'=> '166', 'question'=>  'pain-left-hand'],

            // Swelling attributes
            ['name'=> 'view', 'value'=> 'exam.das28.drawing', 'question'=>  'swelling-skel'],
            ['name'=> 'placeholders', 'value'=> '{xc: 27.5, yc: 76, r: 10}, {xc: 100.5, yc: 76, r: 10}, {xc: 12, yc: 132, r: 10}, {xc: 110, yc: 132, r: 10}, {xc: 10.5, yc: 178, r: 10}, {xc: 125, yc: 175.5, r: 10}, {xc: 51, yc: 253, r: 10}, {xc: 72, yc: 253, r: 9}', 'question'=>  'swelling-skel'],
            ['name'=> 'width', 'value'=> '150', 'question'=>  'swelling-skel'],
            ['name'=> 'height', 'value'=> '352', 'question'=>  'swelling-skel'],

            ['name'=> 'view', 'value'=> 'exam.das28.drawing', 'question'=>  'swelling-right-hand'],
            ['name'=> 'placeholders', 'value'=> '{xc: 130, yc: 20, r: 8}, {xc: 155, yc: 50, r: 8}, {xc: 32, yc: 60, r: 8}, {xc: 55, yc: 67, r: 8}, {xc: 93, yc: 76, r: 8}, {xc: 22, yc: 75, r: 8}, {xc: 45, yc: 83, r: 8}, {xc: 86, yc: 95, r: 8}, {xc: 22, yc: 95, r: 8}, {xc: 45, yc: 103, r: 8}, {xc: 86, yc: 115, r: 8}, {xc: 40, yc: 122, r: 8}, {xc: 60, yc: 131, r: 8}, {xc: 88, yc: 140, r: 8}', 'question'=>  'swelling-right-hand'],
            ['name'=> 'width', 'value'=> '200', 'question'=>  'swelling-right-hand'],
            ['name'=> 'height', 'value'=> '166', 'question'=>  'swelling-right-hand'],

            ['name'=> 'view', 'value'=> 'exam.das28.drawing', 'question'=>  'swelling-left-hand'],
            ['name'=> 'placeholders', 'value'=> '{xc: 130-60, yc: 20, r: 8}, {xc: 155-110, yc: 50, r: 8}, {xc: 32+135, yc: 60, r: 8}, {xc: 55+90, yc: 67, r: 8}, {xc: 93+15, yc: 76, r: 8}, {xc: 22+156, yc: 75, r: 8}, {xc: 45+110, yc: 83, r: 8}, {xc: 86+30, yc: 95, r: 8}, {xc: 22+158, yc: 95, r: 8}, {xc: 45+108, yc: 103, r: 8}, {xc: 86+30, yc: 115, r: 8}, {xc: 40+120, yc: 122, r: 8}, {xc: 60+80, yc: 131, r: 8}, {xc: 88+25, yc: 140, r: 8}', 'question'=>  'swelling-left-hand'],
            ['name'=> 'width', 'value'=> '200', 'question'=>  'swelling-left-hand'],
            ['name'=> 'height', 'value'=> '166', 'question'=>  'swelling-left-hand'],

            // VSG attributes
            ['name'=> 'view', 'value'=> 'exam.das28.question', 'question'=>  'vsg-question'],
            ['name'=> 'type', 'value'=> 'range', 'question'=> 'vsg-question'],
            ['name'=> 'initial-value', 'value'=> 0, 'question'=> 'vsg-question'],
            ['name'=> 'step', 'value'=> 1, 'question'=> 'vsg-question'],
            ['name'=> 'units', 'value'=> 'mm/h', 'question'=> 'vsg-question'],
            ['name'=> 'from', 'value'=> 0, 'question'=> 'vsg-question'],
            ['name'=> 'to', 'value'=> 50, 'question'=> 'vsg-question'],

            // CRP attributes
            ['name'=> 'view', 'value'=> 'exam.das28.question', 'question'=>  'crp-question'],
            ['name'=> 'type', 'value'=> 'range', 'question'=> 'crp-question'],
            ['name'=> 'initial-value', 'value'=> 0, 'question'=> 'crp-question'],
            ['name'=> 'step', 'value'=> 1, 'question'=> 'crp-question'],
            ['name'=> 'from', 'value'=> 0, 'question'=> 'crp-question'],
            ['name'=> 'to', 'value'=> 100, 'question'=> 'crp-question'],

            // BASDAI
            ['name'=> 'view', 'value'=> 'exam.slider-question', 'question'=>  'fatique-question'],
            ['name'=> 'type', 'value'=> 'range', 'question'=> 'fatique-question'],
            ['name'=> 'initial-value', 'value'=> 0, 'question'=> 'fatique-question'],
            ['name'=> 'step', 'value'=> 1, 'question'=> 'fatique-question'],
            ['name'=> 'from', 'value'=> 0, 'question'=> 'fatique-question'],
            ['name'=> 'to', 'value'=> 10, 'question'=> 'fatique-question'],

            ['name'=> 'view', 'value'=> 'exam.slider-question', 'question'=>  'pain-question'],
            ['name'=> 'type', 'value'=> 'range', 'question'=> 'pain-question'],
            ['name'=> 'initial-value', 'value'=> 0, 'question'=> 'pain-question'],
            ['name'=> 'step', 'value'=> 1, 'question'=> 'pain-question'],
            ['name'=> 'from', 'value'=> 0, 'question'=> 'pain-question'],
            ['name'=> 'to', 'value'=> 10, 'question'=> 'pain-question'],

            ['name'=> 'view', 'value'=> 'exam.slider-question', 'question'=>  'swelling-question'],
            ['name'=> 'type', 'value'=> 'range', 'question'=> 'swelling-question'],
            ['name'=> 'initial-value', 'value'=> 0, 'question'=> 'swelling-question'],
            ['name'=> 'step', 'value'=> 1, 'question'=> 'swelling-question'],
            ['name'=> 'from', 'value'=> 0, 'question'=> 'swelling-question'],
            ['name'=> 'to', 'value'=> 10, 'question'=> 'swelling-question'],

            ['name'=> 'view', 'value'=> 'exam.slider-question', 'question'=>  'touch-question'],
            ['name'=> 'type', 'value'=> 'range', 'question'=> 'touch-question'],
            ['name'=> 'initial-value', 'value'=> 0, 'question'=> 'touch-question'],
            ['name'=> 'step', 'value'=> 1, 'question'=> 'touch-question'],
            ['name'=> 'from', 'value'=> 0, 'question'=> 'touch-question'],
            ['name'=> 'to', 'value'=> 10, 'question'=> 'touch-question'],

            ['name'=> 'view', 'value'=> 'exam.slider-question', 'question'=>  'wakeup-question'],
            ['name'=> 'type', 'value'=> 'range', 'question'=> 'wakeup-question'],
            ['name'=> 'initial-value', 'value'=> 0, 'question'=> 'wakeup-question'],
            ['name'=> 'step', 'value'=> 1, 'question'=> 'wakeup-question'],
            ['name'=> 'from', 'value'=> 0, 'question'=> 'wakeup-question'],
            ['name'=> 'to', 'value'=> 10, 'question'=> 'wakeup-question'],

            ['name'=> 'view', 'value'=> 'exam.slider-question', 'question'=>  'shiftness-question'],
            ['name'=> 'type', 'value'=> 'range', 'question'=> 'shiftness-question'],
            ['name'=> 'initial-value', 'value'=> 0, 'question'=> 'shiftness-question'],
            ['name'=> 'step', 'value'=> 1, 'question'=> 'shiftness-question'],
            ['name'=> 'from', 'value'=> 0, 'question'=> 'shiftness-question'],
            ['name'=> 'to', 'value'=> 10, 'question'=> 'shiftness-question'],
        ]);

        $elements->each(function($item){

            $q= App\Question::where('name', $item['question'])->first();
            $attribute= new App\Attribute;
            $data= collect($item)->except(['question'])->toArray();
            $attribute->fill($data);
            $attribute->save();
            $attribute->questions()->attach($q);
        });
    }
}
