<?php

use Illuminate\Database\Seeder;

class ExamTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();

        $elements= collect([
                    ['name'=> 'das28', 'title'=> 'Das28'],
                    ['name'=> 'basdai', 'title'=> 'Basdai'],
                    ['name'=> 'basfi', 'title'=> 'Basfi'],
                    ['name'=> 'haq', 'title'=> 'Haq'],
                    ['name'=> 'pasi', 'title'=> 'Pasi'],
                ]);

        $elements->each(function($item){

            App\Exam::create($item);
        });

    }
}
