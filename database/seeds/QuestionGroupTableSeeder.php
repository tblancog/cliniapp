<?php

use Illuminate\Database\Seeder;

class QuestionGroupTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $elements = collect([
            // Das28 question  groups
            ['name' => 'das28-pain', 'title' => 'Dolor', 'exam' => 'das28'],
            ['name' => 'das28-swelling', 'title' => 'Inflamación', 'exam' => 'das28'],
            ['name' => 'das28-vsg', 'title' => 'VSG', 'exam' => 'das28'],
            ['name' => 'das28-crp', 'title' => 'CRP', 'exam' => 'das28'],

            // Basdai question groups
            ['name' => 'basdai-fatique', 'title' => 'Fatiga', 'exam' => 'basdai'],
            ['name' => 'basdai-pain', 'title' => 'Dolor', 'exam' => 'basdai'],
            ['name' => 'basdai-swelling', 'title' => 'Inflamación', 'exam' => 'basdai'],
            ['name' => 'basdai-touch', 'title' => 'Toque', 'exam' => 'basdai'],
            ['name' => 'basdai-wakeup', 'title' => 'Levantarse', 'exam' => 'basdai'],
            ['name' => 'basdai-shiftness', 'title' => 'Rigidez', 'exam' => 'basdai'],

        ]);


        $elements->each(function ($item) {

            $exam = App\Exam::where('name', $item['exam'])->first();
            $qg = new App\QuestionGroup;
            $item= collect($item)->forget('exam')->toArray();
            $qg->fill($item)->exam()->associate($exam)->save();
        });
    }
}