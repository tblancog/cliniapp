<?php

use Illuminate\Database\Seeder;

class QuestionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Eloquent::unguard();

        $elements= collect([
            // Das28 Pain questions
            ['name' => 'pain-skel', 'text'=> 'Seleccione dónde está el dolor', 'remark'=> 'Cuerpo en general', 'questiongroup'=> 'das28-pain'],
            ['name'=> 'pain-right-hand', 'text'=> 'Seleccione dónde está el dolor', 'remark'=> 'Mano derecha', 'questiongroup'=> 'das28-pain'],
            ['name'=> 'pain-left-hand', 'text'=> 'Seleccione dónde está el dolor', 'remark'=> 'Mano izquierda', 'questiongroup'=> 'das28-pain'],

            // Das28 Swelling questions
            ['name'=> 'swelling-skel', 'text'=> 'Seleccione dónde hay inflamaciones', 'remark'=> 'Cuerpo en general', 'questiongroup'=> 'das28-swelling'],
            ['name'=> 'swelling-right-hand', 'text'=> 'Seleccione dónde hay inflamaciones', 'remark'=> 'Mano derecha', 'questiongroup'=> 'das28-swelling'],
            ['name'=> 'swelling-left-hand', 'text'=> 'Seleccione dónde hay inflamaciones', 'remark'=> 'Mano izquierda', 'questiongroup'=> 'das28-swelling'],

            // Das28 Vsg questions
            ['name'=> 'vsg-question', 'text'=> 'Velocidad de segmentación globular o eritrosedimentación', 'remark'=> 'Sólo valores del 1 al 50', 'questiongroup'=> 'das28-vsg'],

            // Das28 CRP questions
            ['name'=> 'crp-question', 'text'=> 'Estimación global del paciente', 'remark'=> '0: Muy bien, 100: Muy mal', 'questiongroup'=> 'das28-crp'],

            // Basdai questions
            ['name'=> 'fatique-question', 'text'=> '¿Qué tanta fatiga o cansancio ha tenido?', 'remark'=> '0: Nada, 10: Mucho', 'questiongroup'=> 'basdai-fatique'],
            ['name'=> 'pain-question', 'text'=> '¿Qué tanto dolor ha experimentado en el cuello, espalda o caderas debido a la espondilitis?', 'remark'=> '0: Nada, 10: Mucho', 'questiongroup'=> 'basdai-pain'],
            ['name'=> 'swelling-question', 'text'=> 'Inflamación que ha tenido usted en las otras articulaciones (sin contar, cuello, espalda y caderas,)', 'remark'=> '0: Nada, 10: Mucho', 'questiongroup'=> 'basdai-swelling'],
            ['name'=> 'touch-question', 'text'=> '¿Cuánto malestar ha experimentado en las partes de su cuerpo al tocarlas o presionarlas?', 'remark'=> '0: Nada, 10: Mucho', 'questiongroup'=> 'basdai-touch'],
            ['name'=> 'wakeup-question', 'text'=> '¿Cuánta rigidez matutina ha tenido usted al despertarse?', 'remark'=> '0: Nada, 10: Mucho', 'questiongroup'=> 'basdai-wakeup'],
            ['name'=> 'shiftness-question', 'text'=> '¿Cuánto tiempo le dura la rigidez matutina al levantarse?', 'remark'=> '0: Nada, 10: Mucho', 'questiongroup'=> 'basdai-shiftness'],
        ]);

        $elements->each(function($item){

            $qg= App\QuestionGroup::where('name', $item['questiongroup'])->first();
            $q= new App\Question;
            $item= collect($item)->forget('questiongroup')->toArray();
            $q->fill($item)->questiongroup()->associate($qg)->save();
        });
    }
}
