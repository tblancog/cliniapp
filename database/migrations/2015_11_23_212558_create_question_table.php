<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('question', function (Blueprint $table) {

            $table->increments('id');
            $table->string('name')->unique();
            $table->text('text');
            $table->string('remark')->nullable();
            $table->integer('questiongroup_id')->unsigned()->index();
            $table->timestamps();
        });

        Schema::table('question', function (Blueprint $table){

            $table->foreign('questiongroup_id')->references('id')->on('questiongroup')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('question');
    }
}
