<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttributePivotTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attribute_pivot', function (Blueprint $table) {
            $table->integer('attribute_id')->unsigned()->index();
            $table->integer('question_id')->unsigned()->index();
            $table->timestamps();
        });

        Schema::table('attribute_pivot', function(Blueprint $table) {

            $table->foreign('attribute_id')->references('id')->on('attribute')->onDelete('cascade');
            $table->foreign('question_id')->references('id')->on('question')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('attribute_pivot');
    }
}
