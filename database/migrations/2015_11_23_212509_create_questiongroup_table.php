<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestiongroupTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questiongroup', function(Blueprint  $table){

            $table->increments('id')->unsigned();
            $table->string('name');
            $table->string('title');
            $table->integer('exam_id')->unsigned()->index();
            $table->timestamps();
        });

        Schema::table('questiongroup', function(Blueprint $table){

            $table->foreign('exam_id')->references('id')->on('exam')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questiongroup');
    }
}
