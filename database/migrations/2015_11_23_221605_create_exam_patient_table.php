<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExamPatientTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('exam_patient', function(Blueprint $table){

            $table->integer('exam_id')->unsigned()->index();

            $table->integer('patient_id')->unsigned()->index();

            $table->integer('user_id')->unsigned()->index();

            $table->decimal('score', 5,2);
            $table->timestamps();
        });

        Schema::table('exam_patient', function(Blueprint $table){

            $table->foreign('exam_id')->references('id')->on('exam')->onDelete('cascade');
            $table->foreign('patient_id')->references('id')->on('patient')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('exam_patient');
    }
}
