<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;

class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function reset($id)
    {

        // dd($user); die;
        $user = User::find($id);

        $password = $users->generatePassword();
        $user->password = Hash::make($password);

        $user->save();


        Mail::send('emails.email', compact('user', 'password'), function($message) use($user) {
            $message->to($user->email, $user->name)->subject('Login Details');
        });


        $users = User::all();

        return View::make('users::index', compact('users'));


    }
}
