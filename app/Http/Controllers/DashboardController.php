<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
//use App\Exam;

class DashboardController extends Controller
{

//    private $exam;

    /**
     * DashboardController constructor.
     */
    public function __construct()
    {
        $this->middleware('auth',['only'=> 'index']);
//        $this->exam= $exam;
    }

    public function index(Request $request){

        return view('pages.dashboard');
    }

}
