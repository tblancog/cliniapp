<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

$router->bind('exam', function($name){
	return App\Exam::where('name', $name)->first();
});

$router->resource('exam', 'ExamController', [
	// 'only' => ['index', 'show', 'edit', 'update']
]);

$router->get('users', 'UserController@index');

// Dashboard
$router->get('/', ['uses'=> 'DashboardController@index',
	 			   'as'=> 'dashboard.index']);

/* Auth */
// Authentication routes...
//Route::get('login', 'Auth\AuthController@getLogin');
//Route::get('auth/login', 'Auth\AuthController@getLogin');
//Route::post('auth/login', 'Auth\AuthController@postLogin');
//Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
//Route::get('auth/register', 'Auth\AuthController@getRegister');
//Route::post('auth/register', 'Auth\AuthController@postRegister');

// Forgot password
// $router->get('password/email', 'Auth\PasswordController');

$router->controllers([

	'auth'=> 'Auth\AuthController',
	'password'=> 'Auth\PasswordController',
]);

/* Pages */
$router->get('page/{name}',  ['uses'=> 'PageController@index', 'as'=> 'page.terms' ]);