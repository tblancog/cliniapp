<?php

function delete_form($routeParams, $label= 'Eliminar'){

	$form= Form::open(['method'=> 'DELETE', 'route'=> $routeParams]);
	$form.= Form::submit($label, ['class'=> 'btn btn-danger']);
	return $form .= Form::close();

}