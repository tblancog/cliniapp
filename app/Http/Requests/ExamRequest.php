<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ExamRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // return false;
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch($this->method()){
            case 'POST':
            {
                $rules[]= [
                    'name' => 'required|unique:exam,name',
                    'title' => 'required'
                ];
            }
            case 'PUT':
            case 'PATCH':
            {
                $rules[]= [
                    'name' => 'required|unique:exam,name,'.$this->exams->id,
                    'title' => 'required'
                ];   
            }
            defualt: break;
        }
        return $rules;
    }
}
