<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Patient extends Model
{
    protected $table= 'patient';
    protected $fillable= [ 'username', 'name', 'lastname', 'birthday'];
    protected $dates= ['birthday'];

    public function setBirthdayAttribute($date){

    	$this->attributes['birthday']= Carbon::createFromFormat('d/m/Y', $date);
    }

    public function exams()	{

    	return $this->belongsToMany('App\Exam', 'exam_patient')->withPivot('user_id');
    }
}
