<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $table= 'question';
    protected $fillable= [ 'name', 'title', 'text'];

    public function questiongroup(){

        return $this->belongsTo('App\QuestionGroup', 'questiongroup_id');
    }

    public function attributes(){

        return $this->belongsToMany('App\Attribute', 'attribute_pivot')->withPivot('attribute_id');
    }

    public function attributeList(){

        $attributes = $this->attributes()->get();
        $myAttributes= [];

        if($attributes->count() > 0){

            foreach($attributes as $item){

                $myAttributes[$item->name]= $item->value;
            }
        }

        return collect($myAttributes);
    }
}
