<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class QuestionGroup extends Model
{
    protected $table= 'questiongroup';

    protected $fillable= ['name', 'title'];

    public function exam(){

        return $this->belongsTo('App\Exam');
    }

    public function questions(){

        return $this->hasMany('App\Question',  'questiongroup_id');
    }
}
