<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Attribute extends Model
{
    protected $table= 'attribute';
    protected $fillable= ['name', 'value'];

    public function questions(){

        return $this->belongsToMany('App\Question', 'attribute_pivot')->withPivot('question_id');
    }
}
