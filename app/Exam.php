<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Exam extends Model
{
    protected $table= 'exam';
    protected $fillable= [ 'name', 'title'];

    public function patients()
    {

    	return $this->belongsToMany('App\Patient', 'exam_patient')->withPivot('user_id');
    }

    public function users()
    {
    	
        return $this->belongstoMany('App\User', 'exam_patient')->withPivot('patient_id');
    }

    public function questiongroup()
    {
        return $this->hasMany('App\QuestionGroup');
    }

}

