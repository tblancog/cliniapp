/* Left hand script */
var shapes= new Array();
var circles_l_hand_p = new Array({xc: 130-60, yc: 20, r: 8}, {xc: 155-110, yc: 50, r: 8}, {xc: 32+135, yc: 60, r: 8}, {xc: 55+90, yc: 67, r: 8}, {xc: 93+15, yc: 76, r: 8}, {xc: 22+156, yc: 75, r: 8}, {xc: 45+110, yc: 83, r: 8}, {xc: 86+30, yc: 95, r: 8}, {xc: 22+158, yc: 95, r: 8}, {xc: 45+108, yc: 103, r: 8}, {xc: 86+30, yc: 115, r: 8}, {xc: 40+120, yc: 122, r: 8}, {xc: 60+80, yc: 131, r: 8}, {xc: 88+25, yc: 140, r: 8} ); for (var i = 0; i < circles_l_hand_p.length; i++) {
    shapes[i]= left_hand.circle(circles_l_hand_p[i].xc, circles_l_hand_p[i].yc, circles_l_hand_p[i].r);
    shapes[i].attr(style);
    shapes[i].id= "left_hand"+i;
    shapes[i].click(function(){
        if(this.attr('fill')== "#990000"){
            pj--;
            this.animate({fill: "#FEEA3E"}, 200);
        }else{
            pj++;
            this.animate({fill: "#990000"}, 200);
        }
    });
};
