/* ====== SWELLING =========
 Draw skeleton and both hands for swelling*/
// Skeleton script
var shapes= new Array();
var circles_body_i = new Array({xc: 27.5, yc: 76, r: 10}, {xc: 100.5, yc: 76, r: 10}, {xc: 12, yc: 132, r: 10}, {xc: 110, yc: 132, r: 10}, {xc: 10.5, yc: 178, r: 10}, {xc: 125, yc: 175.5, r: 10}, {xc: 51, yc: 253, r: 10}, {xc: 72, yc: 253, r: 9});
for (var i = 0; i < circles_body_i.length; i++) {
    shapes[i]= skeleton_inf_i.circle(circles_body_i[i].xc, circles_body_i[i].yc, circles_body_i[i].r);
    shapes[i].attr(style);
    shapes[i].id= "skeleton_i"+i;
    shapes[i].click(function(){
        if(this.attr('fill')== "#990000"){
            sj--;
            this.animate({fill: "#FEEA3E"}, 200);
        }else{
            sj++;
            this.animate({fill: "#990000"}, 200);
        }
    });
};

// /* Right hand script */
var shapes= new Array();
var circles_r_hand_i = new Array({xc: 130, yc: 20, r: 8}, {xc: 155, yc: 50, r: 8}, {xc: 32, yc: 60, r: 8}, {xc: 55, yc: 67, r: 8}, {xc: 93, yc: 76, r: 8}, {xc: 22, yc: 75, r: 8}, {xc: 45, yc: 83, r: 8}, {xc: 86, yc: 95, r: 8}, {xc: 22, yc: 95, r: 8}, {xc: 45, yc: 103, r: 8}, {xc: 86, yc: 115, r: 8}, {xc: 40, yc: 122, r: 8}, {xc: 60, yc: 131, r: 8}, {xc: 88, yc: 140, r: 8} ); for (var i = 0; i < circles_r_hand_i.length; i++) {
    shapes[i]= right_hand_inf_i.circle(circles_r_hand_i[i].xc, circles_r_hand_i[i].yc, circles_r_hand_i[i].r);
    shapes[i].attr(style);
    shapes[i].id= "right_hand"+i;
    shapes[i].click(function(){
        if(this.attr('fill')== "#990000"){
            sj--;
            this.animate({fill: "#FEEA3E"}, 200);
        }else{
            sj++;
            this.animate({fill: "#990000"}, 200);
        }
    });
};

// /* Left hand script */
var shapes= new Array();
var circles_l_hand_i = new Array({xc: 130-60, yc: 20, r: 8}, {xc: 155-110, yc: 50, r: 8}, {xc: 32+135, yc: 60, r: 8}, {xc: 55+90, yc: 67, r: 8}, {xc: 93+15, yc: 76, r: 8}, {xc: 22+156, yc: 75, r: 8}, {xc: 45+110, yc: 83, r: 8}, {xc: 86+30, yc: 95, r: 8}, {xc: 22+158, yc: 95, r: 8}, {xc: 45+108, yc: 103, r: 8}, {xc: 86+30, yc: 115, r: 8}, {xc: 40+120, yc: 122, r: 8}, {xc: 60+80, yc: 131, r: 8}, {xc: 88+25, yc: 140, r: 8} ); for (var i = 0; i < circles_l_hand_i.length; i++) {
    shapes[i]= left_hand_inf_i.circle(circles_l_hand_i[i].xc, circles_l_hand_i[i].yc, circles_l_hand_i[i].r);
    shapes[i].attr(style);
    shapes[i].id= "left_hand"+i;
    shapes[i].click(function(){
        if(this.attr('fill')== "#990000"){
            sj--;
            this.animate({fill: "#FEEA3E"}, 200);
        }else{
            sj++;
            this.animate({fill: "#990000"}, 200);
        }
    });
};