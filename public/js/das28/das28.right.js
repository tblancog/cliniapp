/* Right hand script */
var shapes= new Array();
var circles_r_hand_p = new Array({xc: 130, yc: 20, r: 8},
    {xc: 155, yc: 50, r: 8},
    {xc: 32, yc: 60, r: 8},
    {xc: 55, yc: 67, r: 8},
    {xc: 93, yc: 76, r: 8},
    {xc: 22, yc: 75, r: 8},
    {xc: 45, yc: 83, r: 8},
    {xc: 86, yc: 95, r: 8},
    {xc: 22, yc: 95, r: 8},
    {xc: 45, yc: 103, r: 8},
    {xc: 86, yc: 115, r: 8},
    {xc: 40, yc: 122, r: 8},
    {xc: 60, yc: 131, r: 8},
    {xc: 88, yc: 140, r: 8} );
for (var i = 0; i < circles_r_hand_p.length; i++) {
    shapes[i]= right_hand_p.circle(circles_r_hand_p[i].xc, circles_r_hand_p[i].yc, circles_r_hand_p[i].r);
    shapes[i].attr(style);
    shapes[i].id= "right_hand"+i;
    shapes[i].click(function(){
        if(this.attr('fill')== "#990000"){
            pj--;
            this.animate({fill: "#FEEA3E"}, 200);
        }else{
            pj++;
            this.animate({fill: "#990000"}, 200);
        }
    });
};
