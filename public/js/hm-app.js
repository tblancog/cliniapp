// Update slider values
function updateOutput(element){
    var elementid= element.id || element.name;
    var output= $("input[name="+elementid+"].output, input#"+elementid+".hm-slider");
    var value= element.value;
    output.val(value);
}

function commaSeparateNumber(val){
    while (/(\d+)(\d{3})/.test(val.toString())){
        val = val.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,");
    }
    return val;
}

function incrementNumberAnimation(number, element){
    // Animate the element's value from x to y:
    $({someValue: 0}).animate({someValue: 45000}, {
        duration: 2000,
        easing:'swing', // can be anything
        step: function() { // called on every step
            // Update the element's text with rounded-up value:
            console.log(commaSeparateNumber(Math.round(this.someValue)));
            $(element).text(commaSeparateNumber(Math.round(this.someValue)));
        }
    });
}

/*
* ********* Events ***
* **/
$(function(){
    $('#result').on('click', function(e){
        var result= calculateDas28();
        //incrementNumberAnimation(result, '#score');
        // how many decimal places allows
        var decimal_places = 2;
        var decimal_factor = decimal_places === 0 ? 1 : Math.pow(10, decimal_places);

        $('#score')
            .animateNumber(
            {
                number: result * decimal_factor,
                numberStep: function(now, tween) {
                    var floored_number = Math.floor(now) / decimal_factor,
                        target = $(tween.elem);

                    if (decimal_places > 0) {
                        // force decimal places even if they are 0
                        floored_number = floored_number.toFixed(decimal_places);

                        // replace '.' separator with ','
                        floored_number = floored_number.toString().replace(',', '.');
                    }

                    target.text(floored_number);
                }
            },
            1000
        );

    });
});
//
//    $('#result').on('click', function (e) {
//        conso le.log('this is the click');
//        e.preventDefault();
//    });
//
//});


