@extends('layouts.app')
@section('title') Home :: @parent @stop
@section('content')

    <header>
        <section class="row break-three">
            <section id="title" class="break-three" >
                <section id="humira-title" class="text-center break-four">
                    {!! HTML::image('images/humira_title.png') !!}
                </section>
                <section id="subtitle">
                    <h2>Clinimetrics App</h2>
                </section>
            </section>
        </section>
    </header>
    <main>
        <section class="row">
            <section class="text-center dashboard-icon break-two">
                <section class="col-xs-6 col-lg-6">
                    <section class="img-icon">{!! HTML::image('images/logo_exam.png') !!}</section>
                    <section class="btn hm-btn-default patient-button">Pacientes</section>
                </section>
                <section class="col-xs-6 col-lg-6">
                    <section class="img-icon">{!! HTML::image('images/logo_dr.png') !!}</section>
                    <section class="btn hm-btn-default exam-button">Exámenes</section>
                </section>
            </section>
        </section>
    </main>

@endsection
@stop
