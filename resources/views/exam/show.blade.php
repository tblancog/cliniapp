@extends('layouts.app')
@section('title') Home :: @parent @stop

{{--@include('partials.header')--}}

@section('content')
	<div class="container-fluid break-four">
		<div class="row">
			<div class="col-xs-12 col-lg-6 col-centered">
				<h1><b>{{ $data->get('exam')->title }}</b></h1>
				<section class="content-header">
					<h1>
						Paciente
						<small>
							{!! Form::select('patients', $data->get('patients'), null, ['class'=> 'js-example-basic-single', 'style'=> 'min-width: 200px']) !!}
						</small>
					</h1>
				</section>
				<script type="text/javascript">
					$('.js-example-basic-single').select2({
						placeholder: "Seleccione o busque un paciente"
					});

				</script>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-12 col-lg-6 col-centered">
				@include('exam.tabs.exam-tabs')
			</div>


		</div>
	</div>

@stop