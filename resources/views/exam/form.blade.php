
<div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}" >
	{!! Form::text('name', null, ['class'=> 'form-control', 'placeholder'=> 'Nombre de examen']) !!}
	{!! $errors->first('name', '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
	{!! Form::text('title', null, ['class'=> 'form-control', 'placeholder'=> 'Título a mostrar']) !!}
	{!! $errors->first('title', '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group">
	{!! Form::submit($submitButton, ['class'=> 'form-control btn btn-primary']) !!}
</div>