
@extends('layouts.app')
@section('title') Home :: @parent @stop
@section('content')

    @include('partials.header')

    <main>
        <section id="title" class="break-two">
            <section id="humira-title" class="text-center"></section>
            <section id="subtitle"><h2>Clinimetrics App</h2></section>
        </section>

        <section class="dashboard-menu break-two">
            <section id="patient" class="dashboard-icon text-center">
                {{--<a href="?php echo base_url() ?>patient">--}}
{{--                <a href="{{ link_to_action('#')}}">--}}
                <a href="{{ '#'}}">
                    {{--<section class="img-icon"><img src=" {{ link_to_asset('img/ui/logo_dr.png') }} ?php echo base_url().'assets/img/ui/logo_dr.png'; ?>" alt="Dr Logo"></section>--}}
                    <section class="img-icon"><img src=" {{ link_to_asset('img/ui/logo_dr.png') }} " alt="Dr Logo"></section>
                    <section class="btn btn-default patient-button">Paciente</section>
                </a>
            </section>
            <section id="exams" class="dashboard-icon text-center">
                {{--<a href="?php echo base_url() ?>exams">--}}
                {{--<section class="img-icon"><img src="?php echo base_url().'assets/img/ui/logo_exam.png'; ?>" alt="Logo Exams"></section>--}}
                <section class="img-icon"><img src=" {{ link_to_asset('img/ui/logo_exam.png') }} " alt="Dr Logo"></section>
                <section class="btn btn-default exam-button">Exámenes</section>
                </a>
            </section>
        </section>
    </main>

@endsection
@stop
