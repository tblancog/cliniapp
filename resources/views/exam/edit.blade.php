@extends('layout.master')


@section('content')
	
	<h3>Editar Examen {{ $exam->title }}</h3>

	{!! Form::model($exam,['route'=> ['exams.update', $exam->name], 'method'=> 'PATCH']) !!}
		
		@include('exam.form', ['submitButton'=> 'Guardar Examen'])

	{!! Form::close() !!}

	{!! delete_form(['exams.destroy', $exam->name]) !!}
	
	{!! link_to_route('exams.index', 'Volver') !!}
@stop