<div class="nav-tabs-custom">

@if($data->get('exam')->questiongroup->count())
        <ul class="nav nav-tabs">
            @foreach($data->get('exam')->questiongroup as $key=> $q)

                {{-- First tab active at start --}}
                <li class="{{ $data->get('exam')->questiongroup->first()->name == $q->name ? 'active' :  '' }}">
                    <a href="#{{ $q->name }}" data-toggle="tab">{{ 1+$key.'. '.$q->title }}</a>
                </li>
            @endforeach

            <li id="result">

                <a href="#res" data-toggle="tab" aria-expanded="true">{{ 2+$key }}. Calcular</a>
            </li>
        </ul>

        <div class="tab-content">
            @foreach($data->get('exam')->questiongroup as $qg)
                @include('exam.tabs.tab-content', $qg)
            @endforeach

            {{-- Result tab content --}}
            @include('exam.tabs.result-content', $qg)

        </div>
    @else
        <div class="alert alert-info alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-warning"></i> Aviso</h4>
            No existe ningún exámen disponible.
        </div>
    @endif
</div>
{{--@include('exam.modal')--}}

{{--Modal for charts --}}
{{--@include('exam.modal-chart', ['exam'=> $data->get('exam')->name,--}}
                              {{--'examTitle'=> $data->get('exam')->title--}}
{{--])--}}
