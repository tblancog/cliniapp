{{-- Question content --}}
<div id="{{ $qg->name }}" class="tab-pane {{ $data->get('exam')->questiongroup->first()->name == $qg->name ? 'active' :  '' }}">
    <div class="row">
        <h3 class="text-center"><b>{{ $qg->title }}</b></h3>
    </div>
    <div class="row">
        <div class="col-centered">

            @if($qg->questions->count())
                @foreach($qg->questions as $q)
                    @include($q->attributeList()->get('view'), ['attr'=> $q->attributeList()])
                @endforeach

            @else
                <div class="alert alert-warning">No existen preguntas cargadas</div>
            @endif


        </div>
    </div>
</div>

