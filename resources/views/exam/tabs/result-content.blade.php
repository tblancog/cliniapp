{{--Result tab content--}}
<div id="res" class="tab-pane">
    <div class="row">
        <h3 class="text-center"><b>Resultado</b></h3>
    </div>
    <div class="row">
        <div class="col-centered col-lg-8">
            <!-- small box -->
            <div class="small-box hm-nav">
                <div class="inner">
                    <h3 id="score">0</h3>

                    <p>Índice {{ $data->get('exam')->title }}</p>
                </div>
                <div class="icon">
                    <i class="fa fa-stethoscope"></i>
                </div>
                <a href="#"  class="small-box-footer" data-toggle="modal" data-target="#confirm-delete">
                    Guardar <i class="fa fa-save"></i>
                </a>
                <a href="#" class="small-box-footer" data-toggle="modal" data-target="#{{ $data->get('exam')->name."Modal" }}">
                    Histórico <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
    </div>

</div>