{{-- Question info section --}}
{{--@include('exam.das28.question')--}}


<section class="col-centered text-center">
    <h4>{{ $q->text }}</h4>
    <figure>
        <article id="{{ $q->name }}" class="break-one"></article>
        <figcaption>
            <h5>{{ $q->remark  }}</h5>
        </figcaption>
        {{-- Parameters for Raphael.js drawing --}}
        <script language="javascript">

            var width= "{{ $attr->get('width', 150)  }}";
            var height= "{{ $attr->get('height', 352)  }}";
            var circles= new Array( {{ $attr->get('placeholders', null) }} );
            var image_name= "{{ $q->name }}.png";
            // Common Raphael drawing class
            @include("exam.".$data->get('exam')->name.".rapha-drawing")

            // Raphael drawing object
            var drawing= new RaphaDrawing();
        </script>
    </figure>
</section>

{{-- Exam scripts --}}
@include("exam.".$data->get('exam')->name.".scripts")
