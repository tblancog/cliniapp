<section class="col-centered text-center">
    <h4>{{ $q->text }}</h4>
    <section class="slider">

        <span class="range min-range">{{ $attr->get('from') }}</span>
                <input class="hm-slider"
                       id="{{ $q->name }}"
                       value="{{ $attr->get('initial-value') }}"
                       type="{{ $attr->get('type') }}"
                       min="{{ $attr->get('from') }}"
                       max="{{ $attr->get('to') }}"
                       step="{{ $attr->get('step') }}"
                       oninput="updateOutput(this)"/>
        <span class="range max-range">{{ $attr->get('to') }}</span>

        <section class="remark">
            <section class="form-group">
                <input name="{{ $q->name }}" value="{{ $attr->get('initial-value') }}"  class="output form-control col-centered" oninput="updateOutput(this)" />
            </section>
            <h5>{{ $q->remark  }}</h5>
        </section>

    </section>


</section>