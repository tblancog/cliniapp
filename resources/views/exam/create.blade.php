@extends('layout.master')


@section('content')
	
	<h3>Crear examen</h3>

	{!! Form::open(['route'=> 'exams.store']) !!}
		
		@include('exam.form', ['submitButton'=> 'Crear Examen'])

	{!! Form::close() !!}
	
	{!! link_to_route('exams.index', 'Volver') !!}
@stop