<!-- Modal -->
<div id="{{ $exam.'Modal' }}" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Resultado {{ $data->get('exam')->title }}</h4>
            </div>
            <div class="modal-body">
                <p id="morrisChart">
                    <script language="javascript">
                        $('#{{ $exam.'Modal' }}').on('shown.bs.modal', function () {
                            $(function () {
                                $( "#morrisChart" ).empty();
                                // LINE CHART
                                var line = new Morris.Line({
                                    element: 'morrisChart',
                                    resize: true,
                                    data: [
                                        {y: '2012-02-24', item1: 50},
                                        {y: '2012-02-25', item1: 30}
//                                        {y: '2012-02-26', item1: 35},
//                                        {y: '2012-02-27', item1: 20}
//                                        {y: '2011 Q1', item1: 2666},
//                                        {y: '2011 Q2', item1: 2778},
//                                        {y: '2011 Q3', item1: 4912},
//                                        {y: '2011 Q4', item1: 3767},
//                                        {y: '2012 Q1', item1: 6810},
//                                        {y: '2012 Q2', item1: 5670},
//                                        {y: '2012 Q3', item1: 4820},
//                                        {y: '2012 Q4', item1: 15073},
//                                        {y: '2013 Q1', item1: 10687},
//                                        {y: '2013 Q2', item1: 8432}
                                    ],
                                    xkey: 'y',
                                    ykeys: ['item1'],
                                    labels: ['Item 1'],
                                    lineColors: ['#AF1F5F'],
                                    hideHover: false,
                                    xLabelAngle: 90,
                                    xLabelFormat: function (x) { return new Date(x).toString();
                                     }
                                });


                            });
                        });

                    </script>
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
            </div>
        </div>

    </div>
</div>