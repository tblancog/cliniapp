<div id="footer">
    <div class="container">
        <section class="row">

            <article class="link break-three col-xs-12 col-lg-12 text-center">
                {!! link_to_route('page.terms', 'Información Farmaceútica', ['name'=> 'terms']) !!}
            </article>
        </section>

        {{--<p class="text-center text-uppercase"><a href="">Información Farmacéutica</a></p>--}}
        {{--<p class="text-muted credit"><span style="text-align: left; float: left">&copy; 2015 <a href="#">Laravel
                    5 Starter Site</a></span> <span class="hidden-phone"
                                                    style="text-align: right; float: right">Powered by: <a
                        href="http://laravel.com/" alt="Laravel 5.1">Laravel 5.1</a></span></p>--}}
    </div>
</div>