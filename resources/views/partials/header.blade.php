{{--<header>--}}
    {{--<nav id="uppernav">--}}
        {{--@foreach($exams as $exam)--}}
            {{--<section class="uppernav {{ $exam->name }}">--}}
                {{--{!! link_to_route('exams.show', $exam->title, [$exam->name]) !!}--}}
            {{--</section>--}}
        {{--@endforeach--}}
    {{--</nav>--}}
{{--</header>--}}

@if(Auth::check())
    <nav class="navbar navbar-inverse navbar-exams">
        <ul class="nav navbar-nav col-lg-12 text-center">
            <li> {!! link_to_route('exam.show', 'DAS28', ['exam'=> 'das28']) !!} </li>
            <li> {!! link_to_route('exam.show', 'BASFI', ['exam'=> 'basfi']) !!} </li>
            <li> {!! link_to_route('exam.show', 'BASDAI', ['exam'=> 'basdai']) !!} </li>
            <li> {!! link_to_route('exam.show', 'HAQ', ['exam'=> 'haq']) !!} </li>
            <li> {!! link_to_route('exam.show', 'PASI', ['exam'=> 'pasi']) !!} </li>

            {{--<li class="col-xs-2 col-lg-2"><a href="{{ HTML }}" class="abc">DAS28</a></li>--}}
            {{--<li class="col-xs-3 col-lg-3"><a href="">BASFI</a></li>--}}
            {{--<li class="col-xs-3 col-lg-3"><a href="">BASDAI</a></li>--}}
            {{--<li class="col-xs-2 col-lg-2"><a href="">HAQ</a></li>--}}
            {{--<li class="col-xs-2 col-lg-2"><a href="">PASI</a></li>--}}
        </ul>
    </nav>
@endif