<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>Humira Clinimetrics App</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"/>
	<link rel="stylesheet" href="css/style.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</head>
<body>
	
	<div class="container">
		<h1>Clinimetrics App</h1>
		@yield('upper_bar')

		@yield('content')

		@yield('footer')
	</div>
</body>
</html>





