@extends('layouts.app')

{{-- Web site Title --}}
@section('title') {!!  trans('site/user.login') !!} :: @parent @stop

{{-- Content --}}
@section('content')

    <header>
        <section id="login-title" class="container-fluid">
            <section class="row break-three ">
                <section class="col-lg-12 text-center break-three">
                    {!! HTML::image('images/humira_title.png', "Humira Central", ['class'=> 'img-responsive col-centered'])  !!}
                    <h2>Clinimetrics App</h2>
                </section>
            </section>
        </section>
    </header>

    <main>
        <section class="container-fluid">
            <section class="row">
                <section class="col-lg-4 text-center col-centered">
                    {!! Form::open(array('url' => URL::to('auth/login'), 'method' => 'post', 'files'=> true)) !!}
                    <div class="form-group  {{ $errors->has('email') ? 'has-error' : '' }}">
                        <div class="controls">
                            {!! Form::text('email', null, array('class' => 'form-control text-center input-lg', 'placeholder'=> trans('site/user.e_mail'))) !!}
                            <span class="help-block">{{ $errors->first('email', ':message') }}</span>
                        </div>
                    </div>
                    <div class="form-group  {{ $errors->has('password') ? 'has-error' : '' }}">
                        <div class="controls">
                            {!! Form::password('password', array('class' => 'form-control text-center input-lg', 'placeholder'=> trans('site/user.password'))) !!}
                            <span class="help-block">{{ $errors->first('password', ':message') }}</span>
                        </div>
                    </div>

                    <div class="form-group">
                        <input type="submit" name="" value="Iniciar Sesión" class="btn hm-btn-default hm-button">
                    </div>
                    <div class="form-group">
                        <a href="{{ URL::to('/password/email') }}">¿Olvidaste contraseña?</a>
                    </div>
                    {!! Form::close() !!}
                </section>
            </section>
        </section>
    </main>


@endsection
