<?php

return [
	'login' => 'Entrar',
	'login_to_account' => 'Acceso',
	'submit' => 'Enviar',
	'register' => 'Crear cuenta',
	'remember' => 'Recordar',
	'password' => 'Contraseña',
	'e_mail' => 'Correo',
	'password_confirmation' => 'Confirmación',
	'confirmation_required' => 'Se requiere confirmación',
	'name' => 'Nombre',
	'home' => 'Home',
	'email_required' => 'You need to provide a valid email address',
	'email_unique' => 'Someone else has taken this email address',
	'name_required' => 'You need to provide your name',
	'password_required' => 'You need to provide your password',
	'password_confirmed' => 'You need to provide confirm password',
    'change_password' => 'Change password',

];
